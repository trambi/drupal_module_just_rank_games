<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\evaluator;

use Drupal\just_rank_games\autoscale\Autoscale;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Autoscale class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\autoscale\Autoscale
 * @group just_rank_games
 */
class AutoscaleTest extends UnitTestCase
{
  
    public function testAdd() {
        $this->assertEquals('3', Autoscale::add('1','2'));
        $this->assertEquals('1.5', Autoscale::add('1','0.5'));
        $this->assertEquals('1.5', Autoscale::add('1','0.50'));
        $this->assertEquals('1.5', Autoscale::add('1.01','0.49'));
    }

    public function testSubstract() {
        $this->assertEquals('1', Autoscale::substract('2','1'));
        $this->assertEquals('0.5', Autoscale::substract('1','0.5'));
        $this->assertEquals('0.5', Autoscale::substract('1','0.50'));
        $this->assertEquals('1.5', Autoscale::substract('2.01','0.51'));
    }

    public function testMultiply(){
        $this->assertEquals('6', Autoscale::multiply('2','3'));
        $this->assertEquals('0.06', Autoscale::multiply('0.2','0.3'));
        $this->assertEquals('1', Autoscale::multiply('2','0.5'));
        $this->assertEquals('1', Autoscale::multiply('2','0.50'));
    }

    public function testCompare(){
        $this->assertEquals(-1, Autoscale::compare('2','3'));
        $this->assertEquals(1, Autoscale::compare('3','2'));
        $this->assertEquals(0, Autoscale::compare('2','2'));
        $this->assertEquals(0, Autoscale::compare('0.5','0.50'));
        $this->assertEquals(1, Autoscale::compare('0.5001','0.50'));
        $this->assertEquals(1, Autoscale::compare('-1','-2'));
        $this->assertEquals(-1, Autoscale::compare('-2','-1'));
        $this->assertEquals(1, Autoscale::compare('3','-2'));
        $this->assertEquals(1, Autoscale::compare('3','-4'));
        $this->assertEquals(-1, Autoscale::compare('-2','3'));
        $this->assertEquals(-1, Autoscale::compare('-4','3'));
    }
}
