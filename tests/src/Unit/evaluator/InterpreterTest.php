<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\evaluator;

use Drupal\just_rank_games\evaluator\Interpreter;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Game class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\evaluator\Interpreter
 * @group just_rank_games
 */
class InterpreterTest extends UnitTestCase
{
    // public function TestInterpreter::evaluateEmptyString() {
    //     _, err := Interpreter::evaluate("", make(Context))
    //     expected := "unable to initiate parser: "
    //     if err == nil {
    //         t.Error("Should return an error")
    //     } else if strings.HasPrefix(err.Error(), expected) == false {
    //         t.Errorf("Unexpected error :%v should begin with: %v", err.Error(), expected)
    //     }
    // }
  
    public function testEvaluateWithOneNumber() {
        $this->assertEquals('1', Interpreter::evaluate('1'));
        $this->assertEquals('1.5', Interpreter::evaluate('1.5'));
    }

    public function testEvaluteWithAddition() {
        $this->assertEquals('3', Interpreter::evaluate('1+2'));
        $this->assertEquals('3.5', Interpreter::evaluate('1+2.5'));
        $this->assertEquals('3.5', Interpreter::evaluate('1.5+2'));
        $this->assertEquals('3.5', Interpreter::evaluate('1.25+2.25'));
        $this->assertEquals('6', Interpreter::evaluate('1+2+3'));
    }

    public function testEvaluteWithSubstraction() {
        $this->assertEquals('1', Interpreter::evaluate('3-2'));
        $this->assertEquals('1.05', Interpreter::evaluate('3.3-2.25'));
    }    

    public function testEvaluateToIncrementOneVariable() {
        $context = ['td_1'=>'2'];
        $this->assertEquals('3', Interpreter::evaluate('td_1+1',$context));
    }

    public function testEvaluteWithAdditionAndMultiplication() {
        $this->assertEquals('7', Interpreter::evaluate('1+2*3'));
        $this->assertEquals('14', Interpreter::evaluate('1*2+3*4'));
    }
   
    public function testEvaluateAddTwoVariables() {
        $context = ['td_1'=>'2','td_2'=>'1'];
        $this->assertEquals('1', Interpreter::evaluate('td_1-td_2',$context));
    }

    public function testApplyWithOneNumber() {
        $interpretInteger = new Interpreter('1');
        $this->assertEquals('1',$interpretInteger->apply());
        $interpretFloating = new Interpreter('1.5');
        $this->assertEquals('1.5',$interpretFloating->apply());
    }

    public function testApplyWithVariables() {
        $withTwoVariables = new Interpreter('var1+var2');
        $this->assertEquals('3',$withTwoVariables->apply(['var1'=>'1','var2'=>'2']));
        $this->assertEquals('1.5',$withTwoVariables->apply(['var1'=>'1','var2'=>'0.5']));
    }
    

    // public function testEvaluteUnknownSymbol() {
    //     _, err := Interpreter::evaluate("(", make(Context))
    //     expected := "unable to parse the expression: unexpected token (EOF)"
    //     if err == nil {
    //         t.Errorf("Should return an error\n")
    //     } else if err.Error() != expected {
    //         t.Errorf("error %v is different from expected (%v)", err.Error(), expected)
    //     }
    // }
    
    // public function testEvaluteOnlyPlus() {
    //     _, err := Interpreter::evaluate("+", make(Context))
    //     expected := "unable to parse the expression: unexpected token (PLUS)"
    //     if err == nil {
    //         t.Errorf("Should return an error\n")
    //     } else if err.Error() != expected {
    //         t.Errorf("error \"%v\" is different from expected \"%v\"", err.Error(), expected)
    //     }
    // }
 
    
    // public function testInterpreter::evaluateUnknownVariable() {
    //     context := make(Context)
    //     context["td_1"] = "2"
    //     result, err := Interpreter::evaluate("{unknown}", context)
    //     expected := "unknown variable \"unknown\""
    //     if err == nil {
    //         t.Errorf("Should return an error and returns %v\n", result)
    //     } else if err.Error() != expected {
    //         t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
    //     }
    // }
    
    // public function testInterpreter::evaluateLeftUnknownVariable() {
    //     context := make(Context)
    //     context["td_1"] = "2"
    //     expected := "error in evaluating node value: unknown variable \"unknown\""
    //     result, err := Interpreter::evaluate("{unknown}+1", context)
    //     if err == nil {
    //         t.Errorf("Should return an error and returns %v\n", result)
    //     } else if err.Error() != expected {
    //         t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
    //     }
    // }
    
    // public function testInterpreter::evaluateRightUnknownVariable() {
    //     context := make(Context)
    //     context["td_1"] = "2"
    //     expected := "error in evaluating node value: unknown variable \"unknown\""
    //     result, err := Interpreter::evaluate("1+{unknown}", context)
    //     if err == nil {
    //         t.Errorf("Should return an error and returns %v\n", result)
    //     } else if err.Error() != expected {
    //         t.Errorf("Error %v is different from expected: %v", err.Error(), expected)
    //     }
    // }
    
}
