<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\RankCriteria;
use Drupal\just_rank_games\business\Statement;
use Drupal\just_rank_games\business\GreaterCondition;
use Drupal\just_rank_games\business\EqualCondition;
use Drupal\just_rank_games\business\SimpleGame;
use Drupal\Tests\UnitTestCase;

/**
 * Tests RankCriteria class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\RankCriteria
 * @group just_rank_games
 */
class RankCriteriaTest extends UnitTestCase
{
    /**
     * Tests the RankCriteria::getPoints() method without statement.
     */
    public function testGetPointsWithoutStatement()
    {
        $onlyDefault = new RankCriteria('zero','0');
        $score1LessThanScore2 = new SimpleGame('0', '1');
        $sameScores = new SimpleGame('1', '1');
        $score1GreatThanScore2 = new SimpleGame('2', '1');
        $this->assertEquals(['0','0'], $onlyDefault->getPoints($score1LessThanScore2));
        $this->assertEquals(['0','0'], $onlyDefault->getPoints($sameScores));
        $this->assertEquals(['0','0'], $onlyDefault->getPoints($score1GreatThanScore2));
    }

    /**
     * Tests the RankCriteria::getPoints() method with variables.
     */
    public function testGetPointsWithVariables()
    {
        $onlyDefault = new RankCriteria('diff','score1-score2');
        $score1LessThanScore2 = new SimpleGame('0', '1');
        $sameScores = new SimpleGame('1', '1');
        $score1GreatThanScore2 = new SimpleGame('2', '1');
        $this->assertEquals(['-1','1'], $onlyDefault->getPoints($score1LessThanScore2));
        $this->assertEquals(['0','0'], $onlyDefault->getPoints($sameScores));
        $this->assertEquals(['1','-1'], $onlyDefault->getPoints($score1GreatThanScore2));
    }

    /**
     * Tests the RankCriteria::getPoints() method.
     */
    public function testGetPoints()
    {
        $drawStatement = new Statement(new EqualCondition(), '1');
        $winStatement = new Statement(new GreaterCondition(), '3');
        $classicFootballPoints = new RankCriteria('points','0',[$drawStatement, $winStatement]);
        $score1LessThanScore2 = new SimpleGame('0', '1');
        $sameScores = new SimpleGame('1', '1');
        $score1GreatThanScore2 = new SimpleGame('2', '1');
        $this->assertEquals(['0','3'], $classicFootballPoints->getPoints($score1LessThanScore2));
        $this->assertEquals(['1','1'], $classicFootballPoints->getPoints($sameScores));
        $this->assertEquals(['3','0'], $classicFootballPoints->getPoints($score1GreatThanScore2));
    }

    /**
     * Tests the RankCriteria::nameFor() method.
     */
    public function testNameFor()
    {
        $points = new RankCriteria('points','0');
        $this->assertEquals('points1', $points->nameFor(1));
        $this->assertEquals('points2', $points->nameFor(2));
    }
}
