<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\Statement;
use Drupal\just_rank_games\business\EqualCondition;
use Drupal\just_rank_games\business\SimpleGame;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Statement class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\Statement
 * @group just_rank_games
 */
class StatementTest extends UnitTestCase
{
    /**
     * Tests the EqualCondition::isTrue() method with zero shift.
     */
    public function testGetValue()
    {
        $statement = new Statement(new EqualCondition(), '1');
        $score1LessThanScore2 = new SimpleGame('0', '1');
        $sameScores = new SimpleGame('1', '1');
        $score1GreatThanScore2 = new SimpleGame('2', '1');
        $this->assertEquals(null, $statement->getValue($score1LessThanScore2));
        $this->assertEquals('1', $statement->getValue($sameScores));
        $this->assertEquals(null, $statement->getValue($score1GreatThanScore2));
    }
}
