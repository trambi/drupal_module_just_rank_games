<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\LesserCondition;
use Drupal\just_rank_games\business\SimpleGame;
use Drupal\Tests\UnitTestCase;

/**
 * Tests LesserCondition class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\LesserCondition
 * @group just_rank_games
 */
class LesserConditionTest extends UnitTestCase
{
  /**
   * Tests the LesserCondition::isTrue() method with zero shift.
   */
    public function testIsTrueWithoutShift()
    {
        $condition = new LesserCondition();
        $score0_1 = new SimpleGame('0', '1');
        $score1_1 = new SimpleGame('1', '1');
        $score2_1 = new SimpleGame('2', '1');
        $this->assertEquals(true, $condition->IsTrue($score0_1));
        $this->assertEquals(false, $condition->IsTrue($score1_1));
        $this->assertEquals(false, $condition->IsTrue($score2_1));
    }
    /**
     * Tests the LesserCondition::isTrue() method with shift of one.
     */
    public function testIsTrueWithShiftOfOne()
    {
        $condition = new LesserCondition('1');
        $score0_1 = new SimpleGame('0', '1');
        $score1_1 = new SimpleGame('1', '1');
        $score2_1 = new SimpleGame('2', '1');
        $this->assertEquals(true, $condition->IsTrue($score0_1));
        $this->assertEquals(true, $condition->IsTrue($score1_1));
        $this->assertEquals(false, $condition->IsTrue($score2_1));
    }
    /**
     * Tests the LesserCondition::isTrue() method with shift.
    */
    public function testEqualClauseIsTrueWithShiftOfMinusOne()
    {
        $condition = new LesserCondition('-1');
        $score0_1 = new SimpleGame('0', '1');
        $score1_1 = new SimpleGame('1', '1');
        $score2_1 = new SimpleGame('2', '1');
        $score0_2 = new SimpleGame('0', '2');
        $this->assertEquals(false, $condition->IsTrue($score0_1));
        $this->assertEquals(false, $condition->IsTrue($score1_1));
        $this->assertEquals(false, $condition->IsTrue($score2_1));
        $this->assertEquals(true, $condition->IsTrue($score0_2));
    }
}
