<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\Game;
use Drupal\Tests\UnitTestCase;

/**
 * Tests SimpleGame class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\Game
 * @group just_rank_games
 */
class GameTest extends UnitTestCase
{
    /**
     * Tests the behaviour of SimpleGame.
     */
    public function testRevert()
    {
        $game = new Game('name1','2','name2','3');
        $reverted = $game->revert();
        $this->assertEquals($reverted->getScore2(), $game->getScore1());
        $this->assertEquals($reverted->getName2(), $game->getName1());
        $this->assertEquals($reverted->getScore1(), $game->getScore2());
        $this->assertEquals($reverted->getName1(), $game->getName2());
    }
}
