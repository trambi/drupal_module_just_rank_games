<?php

// Copyright 2022 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\ConditionFabric;
use Drupal\just_rank_games\business\SimpleGame;
use Drupal\Tests\UnitTestCase;

/**
 * Tests ConditionFabric class methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\ConditionFabric
 * @group just_rank_games
 */
class ConditionFabricTest extends UnitTestCase
{
  /**
   * Tests if the equal patterns are well detected.
   */
    public function testEqual()
    {
        
        $game0v1 = new SimpleGame('0', '1');
        $game1v1 = new SimpleGame('1', '1');
        $game2v1 = new SimpleGame('2', '1');
        $condition = ConditionFabric::fromString('score1=score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score1+1=score2');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score1=score2-1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score1=score2+1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score1-1=score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString(' score1 - 1 = score2 ');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2=score1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2+1=score1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2=score1-1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2-1=score1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2=score1+1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
    }
    /**
     * Tests if greater than pattern are well detected.
    */
    public function testGreaterThan()
    {
        $game0v1 = new SimpleGame('0', '1');
        $game1v1 = new SimpleGame('1', '1');
        $game2v1 = new SimpleGame('2', '1');
        $game3v1 = new SimpleGame('3', '1');
        $condition = ConditionFabric::fromString('score1>score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1+1>score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1-1>score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1>score2+1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1>score2-1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2<score1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2<score1+1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2<score1-1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2+1<score1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2-1<score1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(true, $condition->IsTrue($game2v1));
        $this->assertEquals(true, $condition->IsTrue($game3v1));
    }
    /**
     * Tests if greater than pattern are well detected.
    */
    public function testLesserThan()
    {
        $game0v1 = new SimpleGame('0', '1');
        $game1v1 = new SimpleGame('1', '1');
        $game2v1 = new SimpleGame('2', '1');
        $game3v1 = new SimpleGame('3', '1');
        $game0v2 = new SimpleGame('0', '2');
        $condition = ConditionFabric::fromString('score1<score2');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score1+1<score2');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $this->assertEquals(true, $condition->IsTrue($game0v2));
        $condition = ConditionFabric::fromString('score1-1<score2');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1<score2+1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score1<score2-1');
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $this->assertEquals(true, $condition->IsTrue($game0v2));
        $condition = ConditionFabric::fromString('score2>score1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2>score1+1');
        $this->assertEquals(true, $condition->IsTrue($game0v2));
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $condition = ConditionFabric::fromString('score2>score1-1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2+1>score1');
        $this->assertEquals(true, $condition->IsTrue($game0v1));
        $this->assertEquals(true, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
        $this->assertEquals(false, $condition->IsTrue($game3v1));
        $condition = ConditionFabric::fromString('score2-1>score1');
        $this->assertEquals(true, $condition->IsTrue($game0v2));
        $this->assertEquals(false, $condition->IsTrue($game0v1));
        $this->assertEquals(false, $condition->IsTrue($game1v1));
        $this->assertEquals(false, $condition->IsTrue($game2v1));
    }
}
