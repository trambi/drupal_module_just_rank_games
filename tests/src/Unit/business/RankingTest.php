<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\Tests\just_rank_games\Unit\business;

use Drupal\just_rank_games\business\Ranking;
use Drupal\just_rank_games\business\RankCriteria;
use Drupal\just_rank_games\business\Statement;
use Drupal\just_rank_games\business\GreaterCondition;
use Drupal\just_rank_games\business\EqualCondition;
use Drupal\just_rank_games\business\SimpleGame;
use Drupal\just_rank_games\business\Game;
use Drupal\Tests\UnitTestCase;

/**
 * Tests Ranking class static methods.
 *
 * @coversDefaultClass Drupal\just_rank_games\business\Ranking
 * @group just_rank_games
 */
class RankingTest extends UnitTestCase
{
    /**
     * Tests the Ranking::compute() method.
     */
    public function testCompute()
    {
        $drawStatement = new Statement(new EqualCondition(), '1');
        $winStatement = new Statement(new GreaterCondition(), '3');
        $points = new RankCriteria('Points','0', [$drawStatement, $winStatement]);
        $netScore = new RankCriteria('Net','score1-score2');
        $lossByOne = new SimpleGame('0', '1');
        $draw = new SimpleGame('1', '1');
        $winByOne = new SimpleGame('2', '1');
        $expected = [
            [
                'name1'=>'name1',
                'score1'=>'0',
                'points1'=>'0',
                'net1'=>'-1',
                'name2'=>'name2',
                'score2'=>'1',
                'points2'=>'3',
                'net2'=>'1',
            ],[
                'name1'=>'name1',
                'score1'=>'1',
                'points1'=>'1',
                'net1'=>'0',
                'name2'=>'name2',
                'score2'=>'1',
                'points2'=>'1',
                'net2'=>'0',
            ],[
                'name1'=>'name1',
                'score1'=>'2',
                'points1'=>'3',
                'net1'=>'1',
                'name2'=>'name2',
                'score2'=>'1',
                'points2'=>'0',
                'net2'=>'-1',
            ]
        ];
        $ranking = new Ranking([$points,$netScore]);
        $this->assertEquals( $expected, $ranking->compute([$lossByOne,$draw,$winByOne]) );
    }

    public function testRankWithOneGame(){
        $drawStatement = new Statement(new EqualCondition(), '1');
        $winStatement = new Statement(new GreaterCondition(), '3');
        $points = new RankCriteria('Points','0', [$drawStatement, $winStatement]);
        $netScore = new RankCriteria('Net','score1-score2');
        $game = new Game('team1','1','team2','0');
        $expected = [
            'header'=>['Name','Points','Net'],
            'rows' =>[
                ['team1','3','1'],
                ['team2','0','-1']
            ]
        ];
        $this->assertEquals($expected, Ranking::rank([$points,$netScore],[$game]));
    }

    public function testRankWithFourGames(){
        $drawStatement = new Statement(new EqualCondition(), '1');
        $winStatement = new Statement(new GreaterCondition(), '3');
        $points = new RankCriteria('Points','0', [$drawStatement, $winStatement]);
        $netScore = new RankCriteria('Net','score1-score2');
        $game1a = new Game('team1','1','team2','0');
        $game1b = new Game('team3','2','team4','0');
        $game2a = new Game('team1','1','team3','1');
        $game2b = new Game('team2','1','team4','0');
        $expected = [
            'header' => ['Name', 'Points', 'Net'],
            'rows' =>   [
                ['team3','4','2'],
                ['team1','4','1'],
                ['team2','3','0'],
                ['team4','0','-3']
            ]
        ];
        $this->assertEquals($expected, Ranking::rank([$points,$netScore],[$game1a,$game1b,$game2a,$game2b]));
    }

    public function testRankWithStrictEquality(){
        $drawStatement = new Statement(new EqualCondition(), '1');
        $winStatement = new Statement(new GreaterCondition(), '3');
        $points = new RankCriteria('Points','0', [$drawStatement, $winStatement]);
        $netScore = new RankCriteria('Net','score1-score2');
        $games = [];
        $games[] = new Game('team3','1','team4','1');
        $games[] = new Game('team1','1','team2','1');
        $expected = [
            'header' => ['Name', 'Points', 'Net'],
            'rows' =>   [
                ['team1','1','0'],
                ['team2','1','0'],
                ['team3','1','0'],
                ['team4','1','0']
            ]
        ];
        $this->assertEquals($expected, Ranking::rank([$points,$netScore],$games));
    }
}
