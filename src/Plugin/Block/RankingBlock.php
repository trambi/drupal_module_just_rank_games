<?php

namespace Drupal\just_rank_games\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\just_rank_games\business\RankCriteria;
use Drupal\just_rank_games\business\Statement;
use Drupal\just_rank_games\business\ConditionFabric;
use Drupal\just_rank_games\business\Ranking;
use Drupal\just_rank_games\business\Game;

use Drupal\Core\Form\FormStateInterface;

/**
 *  Ranking for a competition block.
 * 
 *  @Block(
 *    id = "just_rank_games_ranking_block", 
 *    admin_label = @Translation("Ranking for a competition"),
 *  )
 */
class RankingBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The Game Storage service
  */
  protected $gameStorage;

  /**
   * The Game Storage service
  */
  protected $competitionStorage;

  /**
   * RankingBlock constructor.
   *
   */
  public function __construct(array $configuration, $plugin_id,$plugin_definition, $competitionStorage,$gameStorage) {
    parent::__construct($configuration,$plugin_id,$plugin_definition);
    $this->competitionStorage = $competitionStorage;
    $this->gameStorage = $gameStorage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,array $configuration, $plugin_id,$plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      \Drupal::entityTypeManager()->getStorage('competition'),
      \Drupal::entityTypeManager()->getStorage('game')
    );
  }

  /**
   * Ranking of the competition.
   *
   * @return array
   *   The table of ranking
   */
  public function build() {
    $competition = $this->configuration['competition'];
    if ($competition === null || count($competition) === 0) {
      $id = 1;
    }else{
      $id = $competition[0]['target_id'];
    }
    
    $games = $this->getGamesByCompetition($id);
    $criterias = $this->getRankingCriteriaByCompetition($id);
    
    $header = ['Name'=>'Name'];
    foreach($criterias as $criteria){
        $name = $criteria->getName();
        $header[$name]=$name;
    }
    $rankings = Ranking::rank($criterias, $games);
    return [
      'ranking'=> [
        '#type' => 'table',
        '#header' => $rankings['header'],
        '#rows' => $rankings['rows'],
      ],
      'details'=> [
        '#theme' => 'details',
        '#value' => [
          '#theme' => 'item_list',
          '#items' => $games,
        ]
      ],
    ];
  }

  private function getGamesByCompetition(int $id){
    $ids = $this->gameStorage->getQuery()->condition('competition',$id)->execute();
    return array_map(function($gameEntity){
        return new Game($gameEntity->getTeam1(),$gameEntity->getScore1(),$gameEntity->getTeam2(),$gameEntity->getScore2());
    },$this->gameStorage->loadMultiple($ids));

  }
  private function getRankingCriteriaByCompetition(int $id){

    //$competition = \Drupal::entityTypeManager()->getStorage('competition')->load($id);
    $competition = $this->competitionStorage->load($id);
    return array_map(function ($rawCriteria){
        $statements = array_map(function ($statement){
            return new Statement(ConditionFabric::fromString($statement['condition']),$statement['expression_if_true']);
        },$rawCriteria->getStatements());
        return new RankCriteria($rawCriteria->getName(), $rawCriteria->getDefaultExpression(),$statements);
    },$competition->getCriterias());

  }

  // /**
  // * {@inheritdoc}
  // */
  // public function defaultConfiguration(){
  //   return [
  //     'competitionId'=>[1]
  //   ];
  // }

  /**
  * {@inheritdoc}
  */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $form['competition'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'competition',
      '#title' => $this->t('Competition'),
      '#description' => $this->t('Select one competition.'),
      '#tags' => TRUE,
      '#weight' => '0',
    ];
    return $form;
  }

  /**
  * {@inheritdoc}
  */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['competition'] = $form_state->getValue('competition');
  }


}
