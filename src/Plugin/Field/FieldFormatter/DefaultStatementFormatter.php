<?php

namespace Drupal\just_rank_games\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'default_statement_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "default_statement_formatter",
 *   label = @Translation("Default statement formatter"),
 *   field_types = {
 *     "statement"
 *   }
 * )
 */
class DefaultStatementFormatter extends FormatterBase {

  use StringTranslationTrait;

  /**
  * {@inheritdoc}
  */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = $this->viewValue($item);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return array
   *   The field value render array.
   */
  protected function viewValue(FieldItemInterface $item) {
    $condition = $item->get('condition')->getValue();
    $expression_if_true = $item->get('expression_if_true')->getValue();
    return [
      '#theme' => 'statement',
      '#condition' => $condition,
      '#expression_if_true' => $expression_if_true
    ];
  }

}