<?php

namespace Drupal\just_rank_games\Plugin\Field\FieldWidget;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of 'default_statement_widget' widget.
 * 
 * @FieldWidget(
 *   id = "default_statement_widget",
 *   label = @Translation("Default statement widget"),
 *   field_types = {
 *     "statement"
 *   }
 * )
 */
class DefaultStatementWidget extends WidgetBase {
    use StringTranslationTrait;
    /**
    * {@inheritdoc}
    */
    public static function defaultSettings(){
        return [
            'condition_size' => 20,
            'expression_if_true_size' => 20,
            'fieldset_state' => 'open',
            'placeholder' => [
                'condition' => '',
                'expression_if_true' =>  '',
            ],
        ] + parent::defaultSettings();
    }

    /**
    * {@inheritdoc}
    */
    public function settingsForm(array $form, FormStateInterface $form_state){
        $elements = [];
        $elements['condition_size'] = [
            '#type' => 'number',
            '#title' => $this->t('Size of condition textfield'),
            '#default_value' => $this->getSettings('condition_size'),
            '#required' => TRUE,
            '#min' => 1,
            '#max' => $this->getFieldSetting('condition_max_length'),
        ];
        $elements['expression_if_true_size'] = [
            '#type' => 'number',
            '#title' => $this->t('Size of expression_if_true textfield'),
            '#default_value' => $this->getSettings('expression_if_true_size'),
            '#required' => TRUE,
            '#min' => 1,
            '#max' => $this->getFieldSetting('expression_if_true_max_length'),
        ];
        $elements['fieldset_state'] = [
            '#type' => 'select',
            '#title' => $this->t('FieldSet default state'),
            '#options' => [
                'open'=> $this->t('Open'),
                'closed'=> $this->t('Closed'),
            ],
            '#default_value' => $this->getSettings('fieldset_state'),
            '#description' => $this->t('The defaut state of fieldset which contains the two statement fields: open or closed'),
        ];
        $elements['placeholder'] = [
            '#type' => 'details',
            '#title' => $this->t('Placeholder'),
            '#description' => $this->t('Text that will be shiwn inside the field until a value is entered. This hint is usually a sample value or a brief description  of the expected format'),
        ];
        $placeholder_settings = $this->getSetting('placeholder');
        $elements['placeholder']['condition'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Condition field'),
            '#default_value' => $placeholder_settings['condition'],
        ];
        $elements['placeholder']['expression_if_true'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Expression if true field'),
            '#default_value' => $placeholder_settings['expression_if_true'],
        ];
        return $elements;
    }

    /**
    * {@inheritdoc}
    */
    public function settingsSummary(){
      $summary = [];
      $summary[] = $this->t('Statement size: @condition (for condition) and @expression_if_true (for expression_if_true)', [
          '@condition' => $this->getSetting('condition_size'),
          '@expression_if_true' => $this->getSetting('expression_if_true_size')]);
      $placeholder_settings = $this->getSetting('placeholder');
      if (!empty($placeholder_settings['condition']) && !empty($placeholder_settings['expression_if_true'])) {
        $placeholder = $placeholder_settings['condition'] . ' ' . $placeholder_settings['expression_if_true'];
        $summary[] = $this->t('Placeholder: @placeholder', ['@placeholder' => $placeholder]);
      }
      $summary[] = $this->t('Fieldset state: @state', ['@state' => $this->getSetting('fieldset_state')]);

      return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element['details'] = [
      '#type' => 'details',
      '#title' => $element['#title'],
      '#open' => $this->getSetting('fieldset_state') == 'open' ? TRUE : FALSE,
      '#description' => $element['#description'],
    ] + $element;

    $placeholder_settings = $this->getSetting('placeholder');

    $element['details']['condition'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Condition'),
        '#default_value' => isset($items[$delta]->condition) ? $items[$delta]->condition : NULL,
        '#size' => $this->getSetting('condition_size'),
        '#placeholder' => $placeholder_settings['condition'],
        '#maxlength' => $this->getFieldSetting('condition_max_length'),
        '#description' => '',
        '#required' => $element['#required'],
      ];

    $element['details']['expression_if_true'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expression if true'),
      '#default_value' => isset($items[$delta]->expression_if_true) ? $items[$delta]->expression_if_true : NULL,
      '#size' => $this->getSetting('expression_if_true_size'),
      '#placeholder' => $placeholder_settings['expression_if_true'],
      '#maxlength' => $this->getFieldSetting('expression_if_true_max_length'),
      '#description' => '',
      '#required' => $element['#required'],
    ];

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value['condition'] = $value['details']['condition'];
      $value['expression_if_true'] = $value['details']['expression_if_true'];
      unset($value['details']);
    }

    return $values;
  }
   
}