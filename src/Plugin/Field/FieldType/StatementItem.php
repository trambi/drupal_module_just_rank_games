<?php

namespace Drupal\just_rank_games\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
* Plugin implementation of 'Statement' field type.
* 
* @FieldType(
*   id = "statement",
*   label = @Translation("Statement"),
*   description = @Translation("Field for storing a statement aka a condition and an expression if the condition if true"),
*   default_widget = "default_statement_widget",
*   default_formatter = "default_statement_formatter"
* )
*/
class StatementItem extends FieldItemBase {
    use StringTranslationTrait;
    /**
    * {@inheritdoc}
    */
    public static function defaultStorageSettings(){
        return [
            'condition_max_length' => 255,
            'expression_if_true_max_length' => 255,
        ] + parent::defaultStorageSettings();
    }
    /**
    * {@inheritdoc}
    */
    public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data){
        $elements = [];
        $elements['condition_max_length'] = [
            '#type' => 'number',
            '#title' =>$this->t('Condition maximum length'),
            '#default_value' => $this->getSetting('condition_max_length'),
            '#required' => TRUE,
            '#description' => $this->t('Maximum length for the condition in characters'),
            '#min'=> '1',
            '#disabled'=> $has_data,
        ];
        $elements['expression_if_true_max_length'] = [
            '#type' => 'number',
            '#title' =>$this->t('Expression if true maximum length'),
            '#default_value' => $this->getSetting('expression_if_true_max_length'),
            '#required' => TRUE,
            '#description' => $this->t('Maximum length for the expression if true in characters'),
            '#min'=> '1',
            '#disabled'=> $has_data,
        ];
        return $elements + parent::storageSettingsForm($form,$form_state,$has_data);
    }

    /**
    * {@inheritdoc}
    */
    public static function schema(FieldStorageDefinitionInterface $field_definition){
        $schema = [
            'columns' => [
                'condition' => [
                    'type' => 'varchar',
                    'length' => (int)$field_definition->getSetting('condition_max_length'),
                ],
                'expression_if_true' => [
                    'type' => 'varchar',
                    'length' => (int)$field_definition->getSetting('expression_if_true_max_length'),
                ],
            ],
        ];
        return $schema;
    }

    /**
    * {@inheritdoc}
    */
    public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition){
        $properties = [
            'condition' => DataDefinition::create('string')->setLabel(t('Condition')),
            'expression_if_true' => DataDefinition::create('string')->setLabel(t('Expression if true')),
        ];
        return $properties;
    }

    /**
    * {@inheritdoc}
    */
    public function getConstraints(){
        $constraints = parent::getConstraints();
        $constraint_manager = \Drupal::typedDataManager()->getValidationConstraintManager();
        $condition_max_length = $this->getSetting('condition_max_length');
        $expression_if_true_max_length = $this->getSetting('expression_if_true_max_length');
        $constraints[] = $constraint_manager->create('ComplexData',[
            'condition' => [
                'Length' => [
                    'max' => $condition_max_length,
                    'maxMessage' => $this->t('% name: may not be longer than @max characters.',[
                        '%name'=> $this->getFieldDefinition()->getLabel().'(condition)',
                        '@max' => $condition_max_length,
                    ]),
                ],
            ],
            'expression_if_true' => [
                'Length' => [
                    'max' => $expression_if_true_max_length,
                    'maxMessage' => $this->t('% name: may not be longer than @max characters.',[
                        '%name'=> $this->getFieldDefinition()->getLabel().'(expression_if_true)',
                        '@max' => $expression_if_true_max_length,
                    ]),
                ],
            ],
        ]);
        return $constraints;
    }
    
    /**
    * {@inheritdoc}
    */
    public static function generateSampleValue(FieldDefinitionInterface $field_definition){
        $values['condition'] = 'score1=score2';
        $values['expression_if_true'] = 1;
        return $values;
    }

    /**
    * {@inheritdoc}
    */
    public function isEmpty(){
        $condition = $this->get('condition')->getValue();
        $expression_if_true = $this->get('expression_if_true')->getValue();
        return $condition === NULL || $condition === '' || $expression_if_true === NULL || $expression_if_true === '';
    }

}
