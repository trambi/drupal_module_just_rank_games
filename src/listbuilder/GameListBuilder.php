<?php

namespace Drupal\just_rank_games\listbuilder;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * EntityListBuilderInterface implementation responsible for the Game entities.
 */
class GameListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Game ID');
    $header['competition'] = $this->t('Competition');
    $header['name'] = $this->t('Name');
    $header['score'] = $this->t('Score');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\just_rank_games\Entity\Game */
    $row['id'] = $entity->id();
    $row['competition'] = Link::fromTextAndUrl(
      $entity->getCompetition()->getName(),
      new Url(
        'entity.competition.canonical', [
          'competition' => $entity->getCompetition()->id(),
        ]
      )
    );
    $row['name'] = Link::fromTextAndUrl(
      $entity->getTeam1().' vs '.$entity->getTeam2(),
      new Url(
        'entity.game.canonical', [
          'game' => $entity->id(),
        ]
      )
    );
    $row['score'] = $entity->getScore1().' - '.$entity->getScore2();
    return $row + parent::buildRow($entity);
  }

}