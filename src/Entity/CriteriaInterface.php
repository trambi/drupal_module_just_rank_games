<?php

namespace Drupal\just_rank_games\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Represents a Criteria entity.
 */
interface CriteriaInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Gets the Criteria name.
   *
   * @return string
   *   The Criteria name.
   */
  public function getName();

  /**
   * Sets the Criteria name.
   *
   * @param string $name
   *   The Criteria name.
   *
   * @return \Drupal\just_rank_games\Entity\CriteriaInterface
   *   The called Criteria entity.
   */
  public function setName(string $name);

  /**
  * Gets the Criteria default expression.
  *
  * @return string
  *   The Criteria default expression.
  */
  public function getDefaultExpression();

  /**
  * Sets the Criteria default expression.
  *
  * @param string $defaultExpression
  *   The Criteria default expression.
  *
  * @return \Drupal\just_rank_games\Entity\CriteriaInterface
  *   The called Criteria entity.
  */
  public function setDefaultExpression(string $defaultExpression);

  /**
  * Gets the Criteria statements.
  *
  * @return array
  *   The Criteria statements.
  */
  public function getStatements();

  /**
  * Sets the Criteria statements.
  *
  * @param array $statements
  *   The Criteria default expression.
  *
  * @return \Drupal\just_rank_games\Entity\CriteriaInterface
  *   The called Criteria entity.
  */
  public function setStatements(array $statements);

  /**
  * Gets the Criteria default expression.
  *
  * @return int $timestamp
  *   The created date.
  */
  public function getCreatedTime();

  /**
  * Sets the Criteria default expression.
  *
  * @param int $timestamp
  *   The created date.
  *
  * @return \Drupal\just_rank_games\Entity\CriteriaInterface
  *   The called Criteria entity.
  */
  public function setCreatedTime(int $timestamp);

}