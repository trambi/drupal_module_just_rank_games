<?php

namespace Drupal\just_rank_games\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Criteria entity.
 *
 * @ContentEntityType(
 *   id = "criteria",
 *   label = @Translation("Criteria"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\just_rank_games\listbuilder\CriteriaListBuilder",
 *     "access" = "Drupal\just_rank_games\Access\CriteriaAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\just_rank_games\Form\CriteriaForm",
 *       "add" = "Drupal\just_rank_games\Form\CriteriaForm",
 *       "edit" = "Drupal\just_rank_games\Form\CriteriaForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *    "route_provider" = {
 *      "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *    }
 *   },
 *   base_table = "criteria",
 *   admin_permission = "manage criteria entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/criteria/{criteria}",
 *     "add-form" = "/admin/structure/criteria/add",
 *     "edit-form" = "/admin/structure/criteria/{criteria}/edit",
 *     "delete-form" = "/admin/structure/criteria/{criteria}/delete",
 *     "collection" = "/admin/structure/criteria",
 *   },
 * )
 */
class Criteria extends ContentEntityBase implements CriteriaInterface {

  use EntityChangedTrait;

  /**
  * {@inheritdoc}
  */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
  * {@inheritdoc}
  */
  public function getDefaultExpression() {
    return $this->get('default_expression')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setDefaultExpression($expression) {
    $this->set('default_expression', $expression);
    return $this;
  }

  /**
  * {@inheritdoc}
  */
  public function getStatements() {
    $statements = [];
    foreach ($this->get('statements') as $statement) {
      $condition = $statement->get('condition')->getValue();
      $expression_if_true = $statement->get('expression_if_true')->getValue();
        $statements[] = ['condition'=>$condition,'expression_if_true'=>$expression_if_true];
    }
    return $statements;
    return $this->get('statements');
  }

  /**
  * {@inheritdoc}
  */
  public function setStatements($statements) {
    $this->set('statements', $statements);
    return $this;
  }

  /**
  * {@inheritdoc}
  */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
  * {@inheritDoc}
  */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the rank criteria.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['default_expression'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Default expression'))
      ->setDescription(t('The default expression of the rank criteria.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['statements'] = BaseFieldDefinition::create('statement')
      ->setLabel(t('Statements'))
      ->setDescription(t('The statements of the rank criteria.'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('view', [
        'type' => 'statement',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'statement',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}