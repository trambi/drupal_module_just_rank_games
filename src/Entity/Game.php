<?php

namespace Drupal\just_rank_games\Entity;

use Drupal\just_rank_games\IGame;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Game entity.
 *
 * @ContentEntityType(
 *   id = "game",
 *   label = @Translation("Game"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\just_rank_games\listbuilder\GameListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\just_rank_games\Access\GameAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\just_rank_games\Form\GameForm",
 *       "add" = "Drupal\just_rank_games\Form\GameForm",
 *       "edit" = "Drupal\just_rank_games\Form\GameForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *    "route_provider" = {
 *      "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *    }
 *   },
 *   base_table = "game",
 *   admin_permission = "view game entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "score1" = "score1",
 *     "score2" = "score2",
 *     "team1" = "team1",
 *     "team2" = "team2",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/game/{game}",
 *     "add-form" = "/admin/structure/game/add",
 *     "edit-form" = "/admin/structure/game/{game}/edit",
 *     "delete-form" = "/admin/structure/game/{game}/delete",
 *     "collection" = "/admin/structure/game",
 *   },
 * )
 */
class Game extends ContentEntityBase{

  use EntityChangedTrait;

  public function getCompetition() {
    return $this->get('competition')->entity;
  }

  public function setCompetition($competition) {
    $this->set('competition', $competition->target_id);
    return $this;
  }

  public function getOpponent1() {
    return $this->get('opponent1')->entity;
  }

  public function getOpponent1Id() {
    return $this->get('opponent1')->target_id;
  }

  public function setOpponent1Id($uid) {
    $this->set('opponent1', $uid);
    return $this;
  }

  public function setOpponent1(UserInterface $account) {
    $this->set('opponent1', $account->id());
    return $this;
  }

  public function getTeam1(): string{
    return $this->get('team1')->value;
  }

  public function setTeam1(string $team): Game {
    $this->set('team1',$team);
    return $this;
  }

  public function getScore1(): string {
    return $this->get('score1')->value;
  }

  public function setScore1(string $score): Game {
    $this->set('score1',$score);
    return $this;
  }

  public function getOpponent2() {
    return $this->get('opponent2')->entity;
  }

  public function getOpponent2Id() {
    return $this->get('opponent2')->target_id;
  }

  public function setOpponent2Id($uid) {
    $this->set('opponent2', $uid);
    return $this;
  }

  public function setOpponent2(UserInterface $account) {
    $this->set('opponent2', $account->id());
    return $this;
  }
  

  public function getTeam2(): string{
    return $this->get('team2')->value;
  }

  public function setTeam2(string $team): Game {
    $this->set('team2',$team);
    return $this;
  }

  public function getScore2(): string{
    return $this->get('score2')->value;
  }

  public function setScore2(string $score): Game {
    $this->set('score2',$score);
    return $this;
  }

  /**
  * {@inheritdoc}
  */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
  * {@inheritDoc}
  */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['competition'] = BaseFieldDefinition::create('entity_reference')
    ->setLabel(t('Competition'))
    ->setCardinality(1)
    ->setDescription(t('The competition in which occurs this game.'))
    ->setSetting('target_type', 'competition')
    ->setSetting('handler', 'default')
    ->setDisplayOptions('view', [
      'label' => 'above',
      'type' => 'author',
      'weight' => 1,
    ])
    ->setDisplayOptions('form', [
      'type' => 'entity_reference_autocomplete',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'match_limit' => 10,
        'size' => 60,
        'placeholder' => '',
      ],
      'weight' => 1,
    ])
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayConfigurable('view', TRUE);

    $fields['opponent1'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('First opponent'))
      ->setCardinality(1)
      ->setDescription(t('The username of the first opponent.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['team1'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Team name 1'))
      ->setDescription(t('The teamname of the first opponent.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['score1'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Score 1'))
      ->setDescription(t('The score of the first opponent.'))
      ->setSettings([
        'max_length' => 63,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' =>4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['opponent2'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Second opponent'))
      ->setCardinality(1)
      ->setDescription(t('The username of the second opponent.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['team2'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Team name 2'))
      ->setDescription(t('The teamname of the second opponent.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);
  

    $fields['score2'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Score 2'))
      ->setDescription(t('The score of the second opponent.'))
      ->setSettings([
        'max_length' => 63,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}