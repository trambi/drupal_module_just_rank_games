<?php

namespace Drupal\just_rank_games\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Competition entity.
 *
 * @ContentEntityType(
 *   id = "competition",
 *   label = @Translation("Competition"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\just_rank_games\listbuilder\CompetitionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\just_rank_games\Access\CompetitionAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\just_rank_games\Form\CompetitionForm",
 *       "add" = "Drupal\just_rank_games\Form\CompetitionForm",
 *       "edit" = "Drupal\just_rank_games\Form\CompetitionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *    "route_provider" = {
 *      "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *    }
 *   },
 *   base_table = "competition",
 *   admin_permission = "manage competition entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/competition/{competition}",
 *     "add-form" = "/admin/structure/competition/add",
 *     "edit-form" = "/admin/structure/competition/{competition}/edit",
 *     "delete-form" = "/admin/structure/competition/{competition}/delete",
 *     "collection" = "/admin/structure/competition",
 *   },
 * )
 */
class Competition extends ContentEntityBase {

  use EntityChangedTrait;

  /**
  * {@inheritdoc}
  */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  public function getCriterias() {
    $criterias = [];
    foreach ($this->get('criterias') as $criteria) {
       if ($criteria->entity) {
        $criterias[] = $criteria->entity;
      }
    }
    return $criterias;
  }
  /**
   * {@inheritdoc}
   */
  public function addCriteria($rid) {
    $criterias = $this->getCriterias();
    $criterias[] = $rid;
    $this->set('criterias', array_unique($criterias));
  }

  /**
   * {@inheritdoc}
   */
  public function removeCriteria($rid) {
    $this->set('criterias', array_diff($this->getCriterias(TRUE), [$rid]));
  }

  /**
  * {@inheritdoc}
  */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
  * {@inheritdoc}
  */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
  * {@inheritDoc}
  */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the competition.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['criterias'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ranking criterias'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDescription(t('Ranking criterias used by competition.'))
      ->setSetting('target_type', 'criteria')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'match_limit' => 10,
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);


    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}