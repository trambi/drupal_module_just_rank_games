<?php

// Copyright 2021-2022 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


namespace Drupal\just_rank_games\autoscale;

class AutoScale{

  protected static function scale(string $value): int{
    $dotIndex = strpos($value,'.');
    return $dotIndex ? strlen($value) - $dotIndex - 1: 0;
  }

  protected static function approximate(string $value, int $scale){
    $dotIndex = strpos($value,'.');
    // No point
    if($dotIndex === false){
      return ($scale === 0) ? $value : $value . '.' . str_repeat('0',$scale);
    }else{
      $actualScale = strlen($value) - 1 - $dotIndex;
      return ($actualScale>$scale) ? substr($value,0,$dotIndex+1+$scale) : $value . str_repeat('0',$scale-$actualScale);
    }
  }

  protected static function simplify(string $value): string{
    return \strpos($value,'.') ? rtrim(rtrim($value,'0'),'.') : $value; 
  }

  public static function add(string $left,string $right):string{
    $leftScale = Autoscale::scale($left);
    $rightScale = Autoscale::scale($right);
    $result = Autoscale::approximate($left + $right, \max($leftScale,$rightScale));
    return Autoscale::simplify($result);
  }

  public static function substract(string $left,string $right):string{
    $leftScale = Autoscale::scale($left);
    $rightScale = Autoscale::scale($right);
    $result = Autoscale::approximate($left - $right, \max($leftScale,$rightScale));
    return Autoscale::simplify($result);
  }

  public static function multiply(string $left,string $right):string{
    $leftScale = Autoscale::scale($left);
    $rightScale = Autoscale::scale($right);
    $result = Autoscale::approximate($left * $right, $rightScale+$leftScale);
    return Autoscale::simplify($result);
  }

  public static function compare(string $left, string $right): int{
    $result = 0;
    $stringCompare = strcmp(Autoscale::simplify($left), Autoscale::simplify($right));
    if ($stringCompare > 0){
      $result = 1;
    }else if ($stringCompare < 0){
      $result = -1;
    }
    return $left[0]==='-' && $right[0]==='-' ? -1 * $result : $result;
  }
}
