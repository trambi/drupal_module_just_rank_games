<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games;

Trait HasTwoNames
{
    /**
    * The first name .
    *
    * @var string
    */
    protected $name1;

    /**
     * The second name.
     *
     * @var string
     */
    protected $name2;

    /**
     * Get first name
     */
    public function getName1(): string
    {
        return $this->name1;
    }

    /**
     * Get second name
     */
    public function getName2(): string
    {
        return $this->name2;
    }
}