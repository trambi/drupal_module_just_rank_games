<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\ICondition;
use Drupal\just_rank_games\IGame;

class Statement
{
    /**
     * condition.
     *
     * @var ICondition
     */
    protected $condition;
    /**
     * expressionIfTrue.
     *
     * @var string
     */
    protected $expressionIfTrue;
    /**
     * Constructs a Statement.
     *
     * @param ICondition $condition
     *   The condition to use for this statement
     * @param string $expressionIfTrue
     *    The expression (to evaluate) to return if the condition is true
     */
    public function __construct(ICondition $condition, string $expressionIfTrue)
    {
        $this->condition = $condition;
        $this->expressionIfTrue = $expressionIfTrue;
    }

    public function getValue(IGame $game)
    {
        if ($this->condition->isTrue($game)) {
            return $this->expressionIfTrue;
        }
        return null;
    }
}
