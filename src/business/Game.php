<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\IGame;
use Drupal\just_rank_games\HasTwoScores;
use Drupal\just_rank_games\HasTwoNames;
use Drupal\just_rank_games\HasTwoIds;

class Game implements IGame
{
    use HasTwoScores, HasTwoNames, HasTwoIds;
    /**
     * Constructs a Game.
     *
     * @param object $name1
     *   Name of the first opponent.
     * @param object $score1
     *   Score of the first opponent.
     * @param string $name2
     *   Name of the second opponent.
     * @param string $score2
     *   Score of the second opponent.
     */
    public function __construct(string $name1, string $score1, string $name2, string $score2)
    {
        $this->score1 = $score1;
        $this->score2 = $score2;
        $this->id1 = $name1;
        $this->name1 = $name1;
        $this->id2 = $name2;
        $this->name2 = $name2;
    }

    /**
     * Return a reverted game
     */
    public function revert(): IGame
    {
        return new Game($this->name2,$this->score2,$this->name1, $this->score1);
    }

    public function __toString(): string{
      return $this->name1." - ".$this->score1." vs ".$this->score2." - ".$this->name2;
    }
}
