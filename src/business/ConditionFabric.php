<?php

// Copyright 2022 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

class ConditionFabric {
    public static function fromString(string $textualCondition) {
        $withoutSpace = str_replace(' ','',$textualCondition);
        if (preg_match("/^score1([+-]\d*){0,1}=score2([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $leftShift=$matches[1];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            if(count($matches) === 3){
                $rightShift = $matches[2];
            }
            return new EqualCondition($rightShift);
        }
        if(preg_match("/^score2([+-]\d*){0,1}=score1([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $rightShift = $matches[1];
            }
            if(count($matches) === 3){
                $leftShift=$matches[2];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            return new EqualCondition($rightShift);
        }
        if (preg_match("/^score1([+-]\d*){0,1}>score2([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $leftShift=$matches[1];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            if(count($matches) === 3){
                $rightShift = $matches[2];
            }
            return new GreaterCondition($rightShift);
        }
        if (preg_match("/^score2([+-]\d*){0,1}<score1([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $rightShift = $matches[1];
            }
            if(count($matches) === 3){
                $leftShift=$matches[2];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            return new GreaterCondition($rightShift);
        }
        if (preg_match("/^score1([+-]\d*){0,1}<score2([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $leftShift=$matches[1];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            if(count($matches) === 3){
                $rightShift = $matches[2];
            }
            return new LesserCondition($rightShift);
        }
        if (preg_match("/^score2([+-]\d*){0,1}>score1([-+]\d*){0,1}$/",$withoutSpace,$matches) === 1){
            $rightShift = '0';
            if (count($matches) === 2){
                $rightShift = $matches[1];
            }
            if(count($matches) === 3){
                $leftShift=$matches[2];
                $leftShift[0] = $leftShift[0] === '-' ? '+':'-';
                $rightShift = $leftShift; 
            }
            return new LesserCondition($rightShift);
        }
        return NULL;
    }
}
