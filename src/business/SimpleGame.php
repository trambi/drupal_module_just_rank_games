<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\IGame;
use Drupal\just_rank_games\HasTwoScores;
use Drupal\just_rank_games\HasTwoNames;
use Drupal\just_rank_games\HasTwoIds;

class SimpleGame implements IGame
{
    use HasTwoScores, HasTwoNames, HasTwoIds;
    /**
     * Constructs a SimpleGame.
     *
     * @param string $score1
     *   The first score.
     * @param string $score2
     *   The second score.
     */
    public function __construct(string $score1, string $score2)
    {
        $this->score1 = $score1;
        $this->score2 = $score2;
        $this->id1 = 'id1';
        $this->name1 = 'name1';
        $this->id2 = 'id2';
        $this->name2 = 'name2';
    }

    /**
     * Return a reverted game
     */
    public function revert(): IGame
    {
        return new SimpleGame($this->score2,$this->score1);
    }
}
