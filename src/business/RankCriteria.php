<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\IRankCriteria;
use Drupal\just_rank_games\IGame;
use Drupal\just_rank_games\evaluator\Interpreter;

class RankCriteria implements IRankCriteria{
  /**
   * Name of the field
   * @var string
   * 
   */
  protected $name;
  /**
   * Array of statements to check
   * .
   *
   * @var array
   */
  protected $statements;
  /**
   * expression if no statements is true.
   *
   * @var string
   */
  protected $defaultExpression;

  /**
   * Constructs a RankCriteria.
   *
   * @param string $name 
   *    Name of the field
   * @param string $defaultExpression
   *    The expression (to evaluate) to return if all statements are false
   * @param array $statements 
   *    The array of statement to check
   */
  public function __construct(string $name, string $defaultExpression,array $statements = []) {
    $this->name = $name;
    $this->defaultExpression = $defaultExpression;
    $this->statements = $statements;
  }

  protected function extractPointsFromValueAndGame(string $value, IGame $game){
    $result = Interpreter::evaluate($value,['score1'=>$game->getScore1(),'score2'=>$game->getScore2()]);
    return $result;
  }

  public function getPoints(IGame $game):array {
    $revertedGame = $game->revert();
    $value1 = $value2 = $this->defaultExpression;
    foreach( $this->statements as $statement){
      $valueOrNull = $statement->getValue($game);
      if($valueOrNull !== null){
        $value1 = $valueOrNull;
      }
      $valueOrNull = $statement->getValue($revertedGame);
      if($valueOrNull !== null){
        $value2 = $valueOrNull;
      }
    }
    return [
      $this->extractPointsFromValueAndGame($value1, $game),
      $this->extractPointsFromValueAndGame($value2, $revertedGame)
    ];
  }

  public function nameFor(int $index):string{
    return lcfirst(ucwords($this->name)).strval($index);
  }

  public function getName():string{
    return $this->name;
  }

  
}