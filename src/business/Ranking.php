<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\autoscale\Autoscale;

class Ranking {
  protected $criterias;
  protected $rankingRows;
  public function __construct(array $rankCriterias){
    $this->criterias = $rankCriterias;
    $this->rankingRows = [];
  }

  public function getHeaders(){
    return array_merge(['Name'],array_map(function ($criteria){
      return $criteria->getName();
    },$this->criterias));
  }

  public function compute(array $games) {
    $extendedGames = [];
    foreach($games as $game){
      $extendedGame = [];
      $extendedGame['score1'] = $game->getScore1();
      $extendedGame['score2'] = $game->getScore2();
      $extendedGame['name1'] = $game->getName1();
      $extendedGame['name2'] = $game->getName2();
      foreach($this->criterias as $criteria){
        [$points1, $points2] = $criteria->getPoints($game);
        $extendedGame[$criteria->nameFor(1)] = $points1;
        $extendedGame[$criteria->nameFor(2)] = $points2;
      }
      $extendedGames[] = $extendedGame;
    }
    return $extendedGames;
  }

  public function sum(array $extendedGames): Ranking {
    $this->rankingRows = [];
    foreach($extendedGames as $game){
      foreach([1,2] as $index){
        $id = $game['name'.$index];
        $keyExists = array_key_exists($id,$this->rankingRows);
        $row = $keyExists ? $this->rankingRows[$id] : [$id];
        foreach($this->criterias as $i=>$criteria){
          $row[1+$i] = $keyExists ? Autoscale::add($row[1+$i],$game[$criteria->nameFor($index)]) : $game[$criteria->nameFor($index)];
        }
        $this->rankingRows[$id] = $row;
      }
    }
    return $this;
  }

  protected function getSortFunction(): callable {
    $criterias = $this->criterias;
    return function ($left, $right) use($criterias): int {
      foreach($criterias as $index=>$criteria){
        $result = - Autoscale::compare($left[1+$index], $right[1+$index]);
        if( $result !== 0){
          return $result;
        }
      }
      return strcmp($left[0],$right[0]);
    };
  }

  public function sort(): array{
    \usort($this->rankingRows,$this->getSortFunction());
    return $this->rankingRows;
  }

  public static function rank(array $rankCriterias, array $games): array{
    $ranking = new Ranking($rankCriterias);
    $extendedGames = $ranking->compute($games);
    return [
      'header'=> $ranking->getHeaders(),
      'rows'=>$ranking->sum($extendedGames)->sort()
    ];
  }

  
}