<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games\business;

use Drupal\just_rank_games\ICondition;
use Drupal\just_rank_games\IGame;
use Drupal\just_rank_games\autoscale\Autoscale;

class GreaterCondition implements ICondition
{
    /**
     * shift.
     *
     * @var string
     */
    protected $rightShift;

    /**
     * Constructs a GreaterCondition.
     *
     * @param string $rightShift
     *   The rightShift of GreaterCondition.
     */
    public function __construct(string $rightShift='0')
    {
        $this->rightShift = $rightShift;
    }

    /**
     * Return true if score1 is greater than score2+rightShift
     * @param IGame $game
     */
    public function isTrue(IGame $game)
    {
        return Autoscale::compare($game->getScore1(), Autoscale::add($game->getScore2(), $this->rightShift)) === 1;
    }
}
