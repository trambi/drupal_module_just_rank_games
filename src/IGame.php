<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

namespace Drupal\just_rank_games;

interface IGame
{
    /**
     * Getter of score1
     */
    public function getScore1(): string;

    /**
     * Getter of score2
     */
    public function getScore2(): string;

    /**
     * Return a reverted game
     */
    public function revert(): IGame;

    /**
     * Return a name for opponent 1
     */
    public function getName1(): string;

    /**
     * Return an id for opponent 1
     */
    public function getId1(): string;

    /**
     * Return a name for opponent 2
     */
    public function getName2(): string;

    /**
     * Return an id for opponent 2
     */
    public function getId2(): string;
}
