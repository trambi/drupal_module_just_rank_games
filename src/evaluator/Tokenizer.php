<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

class Tokenizer {
	protected $text;
    protected $length;
	protected $pos;
	protected $currentToken;
	protected $currentByte;
    public function __construct(string $input){
        $this->length = strlen($input);
        if ($this->length > 0) {
            $this->text = $input;
            $this->pos = 0;
            $this->currentToken = null;
            $this->currentByte = $input[0];
        }else{
            throw new \Exception('unable to create an tokenizer from empty input');
        }
        
    }
    protected function advance(){
        $this->pos++;
	    if ($this->pos >= $this->length) {
		    $this->currentByte = "\0";
	    } else {
		    $this->currentByte = $this->text[$this->pos];
	    }
    }
    protected function skipWhiteSpace() {
        while($this->currentByte != 0 && preg_match('/^\s$/',$this->currentByte) === 1 ){
            $this->advance();
        }
    }

    protected function decimal(): string{
        $result = '';
        while(preg_match('/^[0-9.]$/',$this->currentByte) === 1){
            
            $result .= $this->currentByte;
            $this->advance();
        }
        return $result;
    }

    protected function variable(): string {
        $result = '';
        while(preg_match('/^[a-zA-Z0-9_]$/',$this->currentByte) === 1){
            
            $result .= $this->currentByte;
            $this->advance();
        }
        return $result;
    }
    public function nextToken(): Token {
        while( $this->currentByte !== "\0" ) {
            if ( preg_match('/^\s$/', $this->currentByte) === 1) {
                $this->skipWhiteSpace();
                continue;
            }
            if ( preg_match('/^[0-9]$/', $this->currentByte) === 1) {
                return new Token(TokenType::NUMBER, $this->decimal());
            }
            if ( preg_match('/^[a-zA-Z]$/', $this->currentByte) === 1) {
                return new Token(TokenType::VARIABLE, $this->variable());
            }
            if ($this->currentByte === '+') {
                $this->advance();
                return new Token(TokenType::PLUS, '+');
            }
            if ($this->currentByte === '-') {
                $this->advance();
                return new Token(TokenType::MINUS, '-');
            }
            if ($this->currentByte === '*') {
                $this->advance();
                return new Token(TokenType::MUL, '*');
            }
            if ($this->currentByte === '(') {
                $this->advance();
                return new Token(TokenType::LPAREN, '(');
            }
            if ($this->currentByte === ')') {
                $this->advance();
                return new Token(TokenType::RPAREN, ')');
            }
            throw new \Exception('unknown token: "'.$this->currentByte.'"');
        }
        return new Token(TokenType::EOF, 'EOF');
    }
}
