<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

use Drupal\just_rank_games\autoscale\Autoscale;

// BinOpNode is binary operator node,
// it has left node - first node of the binary operator node
// it has right node - second node of the binary operator node
class BinOpNode implements INode {
	protected $token;
	protected $left;
	protected $right;
	public function __construct(Token $token,INode $left,INode $right){
		$this->token = $token;
		$this->left = $left;
		$this->right = $right;
	}
	public function __toString(): string{
		return sprintf("[%v] <--[%v]--> [%v]", $this->left, $this->token->value, $this->right);
	}
	// getValue return the value of the binary operator apply to left node and right node
	public function getValue(array $context): string {
		$left = $this->left->getValue($context);
		$right= $this->right->getValue($context);
		switch ($this->token->tokenType) {
			case TokenType::PLUS:
				return Autoscale::add($left, $right);
			case TokenType::MINUS:
				return Autoscale::substract($left, $right);
			case TokenType::MUL:
				return Autoscale::multiply($left, $right);
			default:
				throw new \Exception('Unknown operator type '.$this->token->tokenType);
			}
	}
}