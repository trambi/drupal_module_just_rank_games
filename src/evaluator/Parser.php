<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

// Parser is in charge of interpreting the tokens furnished by tokenizer
class Parser {
	protected $tokenizer;
	protected $currentToken;
	// NewParser create an parser from an input string
	public function __construct(string $input){
		$this->tokenizer = new Tokenizer($input);
	}
	protected function eat(string $tokenType) {
		if ($this->currentToken->tokenType === $tokenType) {
			$this->currentToken = $this->tokenizer->nextToken();
		}else{
			throw new \Exception(sprintf('unexpected token (%s)', $this->currentToken->tokenType));
		}
	}

	// term return an NUMBER token value
	protected function term(): INode {
		// term : factor (MUL factor)*
		$node = $this->factor();
		while($this->currentToken->tokenType === TokenType::MUL){
			$op = $this->currentToken;
			if ($op->tokenType === TokenType::MUL) {
				$this->eat(TokenType::MUL);
				$factor = $this->factor();
				$node = new BinOpNode($op, $node, $factor);
			} else {
				throw new \Exception(sprintf('invalid operator! - Token type: %s', $op->tokenType));
			}
		}
		return $node;
	}
	// factor returns an NUMBER token or expr token
	protected function factor(): INode {
		// factor: NUMBER | LPAREN expr RPAREN | LCBRACKET VARIABLE RCBRACKET
		$token = $this->currentToken;
		if ($token->tokenType === TokenType::NUMBER) {
			$this->eat(TokenType::NUMBER);
			return new NumNode($token);
		} else if ($token->tokenType === TokenType::LPAREN) {
			$this->eat(TokenType::LPAREN);
			$node = $this->expr();
			$this->eat(TokenType::RPAREN);
			return $node;
		} else if ($token->tokenType === TokenType::VARIABLE) {
			$token = $this->currentToken;
			$this->eat(TokenType::VARIABLE);
			return new VarNode($token);
		}
		throw new \Exception(sprintf('unknown token for a term: %s', $token->tokenType));
	}
	public function expr(): INode {
		// expr: term ((PLUS|MINUS) term)*
		// term: factor(MUL factor)*
		// factor: NUMBER | LPAREN expr RPAREN
		$this->currentToken = $this->tokenizer->nextToken();
		$node = $this->term();
		while($this->currentToken->tokenType === TokenType::PLUS || $this->currentToken->tokenType === TokenType::MINUS){
			$op= $this->currentToken;
			if ($op->tokenType === TokenType::PLUS || $op->tokenType === TokenType::MINUS ) {
				$this->eat($op->tokenType);
			} else {
				throw new \Exception(sprintf('invalid operator! - Token type: %s', $op->tokenType));
			}
			$term = $this->term();
			$node = new BinOpNode($op, $node, $term);
		}
		return $node;
	}
}



