<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

// VarNode is variable node
class VarNode implements INode {
	protected $token;
	public function __construct(Token $token){
		$this->token = $token;
	}
	// String return a string for the variable node -- useful to debug and test
	public function __toString(): string{
		return '{'.$this->token->value.'}';
	}
	// getValue return value according to context converted into string of variable node
	public function getValue(array $context): string {
		if (array_key_exists($this->token->value,$context)) {
			return $context[$this->token->value];
		}else{
			throw new \Exception('unknown variable "'.$this->token->value.'"');
		}
	}

}