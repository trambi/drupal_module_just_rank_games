<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

class Interpreter{
	protected $ast;
	public function __construct(string $input){
		$parser = new Parser($input);
		$this->ast = $parser->expr();
	}
	public function apply(array $context=[]): string{
		return $this->ast->getValue($context);
	}

	public static function evaluate(string $input,array $context=[]): string{
		return (new Interpreter($input))->apply($context);	
	}
}




