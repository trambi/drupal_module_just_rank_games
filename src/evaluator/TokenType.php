<?php

// Copyright 2021 Bertrand Madet

// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at

// 		http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// Package evaluator is based on the series of article of Ruslan Pivak Blog https://ruslanspivak.com
namespace Drupal\just_rank_games\evaluator;

class TokenType {
	// NUMBER is the token type for integer
	const NUMBER = "NUMBER";
	// PLUS is the token type for the plus symbol '+' used for addition
	const PLUS = "PLUS";
	// MINUS is the token type for the minus symbol '-' for substraction
	const MINUS = "MINUS";
	// MUL is the token type for the star symbol '*' for multiplication
	const MUL = "multiplicity";
	// LPAREN is the token type for the left parenthesis symbol (
	const LPAREN = 'LPAREN';
	// RPAREN is the token type for the right parenthesis symbol (
	const RPAREN = 'RPAREN';
	// VARIABLE is the token type for variable
	const VARIABLE = 'VARIABLE';
	// EOF is the token that indicated there is no more symbol for tokenizer
	const EOF = 'EOF';
}
